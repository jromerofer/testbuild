//const io = require('../io');
const crypt = require ('../crypt');

function loginV2(req, res) {
  console.log("POST /apitechu/v2/users/login");
  console.log("BODY pasado en llamada POSTMAN:");
  //console.log(req.body);
  console.log(req.body.email);
  console.log(req.body.password);

  var email = req.body.email;
  var password = req.body.password;

  //RECUPERA BD
  var query = 'q={“email” : “' + email + '“}';
  httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.lenght > 0) {
          var response = body[0];
          console.log("Usuario encontrado ");
          console.log("comprobar password del usuario ");
          if crypt.checkPassword(req.body.password, response.password) == true) {
            console.log("Password correcta ");

            var putBody = '{"$set":{"logged":true}}';
            //query = 'q={"id": ' +body[0].id + '}';  //esto serí apara usar la id en vez del email

            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              if (errPUT) {
                var response = {
                  "msg" : "Error obteniendo usuario"
                }
                res.status(500);
              } else {
                if (body.lenght > 0) {
                  var response = bodyPUT[0];
                  /**var response = {
                    "msg" : "Usuario no encontrado",
                    "idUsuario" : body[0].id
                    }
                  **/
                  console.log("Logged de usuario";
                  //
                  } else {
                    var response = {
                      "msg" : "Usuario no encontrado"
                      }
                    res.status(404);
                    }
              }
          }
            res.send(response);

          console.log("Logged de usuario a: ";

          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
        }
      }
      res.send(response);
  }


    /**for (var user of users) {

      console.log("ID del usuario: " + user.id);
      console.log("email del usuario: " + user.email);
      console.log("pasword del usuario: " + user.pasword); //OJO el fichero tiene el texto "pasword" no "password"
                                                           //Lo cambio aquí para no rehacer todo el ficherito...

      //ACTUALIZA BD
      httpClient.put(“user?” + query + “&” + mLabAPIKey,

            query = ’q={“id” : “‘ + body[0].id + ‘“}’;
        var query = 'q={"id" :' +  id + '}'; //comilla simple es un string literal. Estamos componiendo la query que le vamos a pasar

      if (user.email == req.body.email && user.pasword.id(crypt.hash) == req.body.password) {

         console.log("Email y Pass de usuario para LOGIN correcto ");

         var putBody = ‘{“$set”:{“logged”:true}}’;
         httpClient.put(“user?” + query + “&” + mLabAPIKey, JSON.parse(putBody),

         console.log("Logged de usuario a: " + user.logged );


         var query = ’q={“email” : “‘ + email + ‘“}’;
         query = ’q={“id” : “‘ + body[0].id + ‘“}’;
         var putBody = ‘{“$set”:{“logged”:true}}’;
         httpClient.put(“user?” + query + “&” + mLabAPIKey, JSON.parse(putBody),

         console.log("Usuario persistido con logged con éxito en fichero");

         resultado.msg = "Login correcto";
         resultado.idUsuario = user.id;

/**
var id = req.params.id;

var query = 'q={"id" :' +  id + '}'; //comilla simple es un string literal. Estamos componiendo la query que le vamos a pasar

var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente Creado");
  httpClient.get("user?" + query + "&" + mLabAPIKey, // concatenamos la consulta
    function(err, resMLab, body) {

      if (err) {
        var response = {
          "msg" : "Error oteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.lenght > 0) {
          var response =body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
        res.status(404);
        }

      }








**/

         // aqui no puedo hacer el res.send({"msg": "Login correcto"}); porque se iría de la ejecución, hay que ponerlo después.
         break;

      } else {
            console.log("No es posible validar el usuario");
            resultado.msg = "Login incorrecto";
      }

    }

    res.send(resultado);

  }


//LOGOUT
function logoutV2(req, res) {
    console.log("POST /apitechu/v1/users/logout");
    console.log("BODY pasado en llamada POSTMAN:");
    console.log(req.body);
    console.log(req.body.id);

    var users = require('../users_pwds.json');
    var resultado = {};

     for (var user of users) {

      console.log("ID del usuario: " + user.id);
      console.log("email del usuario: " + user.email);
      console.log("Pasword del usuario: " + user.pasword);

      // El ejecicio solo pide validar el "ID" pero creo que deberían validarse más parémetros... en el BODY, en este caso solo va el ID.
      // Dejo una posible valicación... OJO con la llave ;-) "{"

      //if (user.email == req.body.email && user.pasword == req.body.password && user.id == req.body.id) {
       if (user.id === req.body.id) {

          console.log("ID de usuario para LOGOUT correcto " + user.id);

          delete user.logged;
          console.log("Eliminado Logged de usuario ID: " + user.id);

          //io.writeUserDataToFile(users);

          console.log("Usuario persistido sin logged con éxito en fichero");
          resultado.msg = "Logout correcto";
          resultado.idUsuario = user.id;

          break;

        } else {
            console.log("Logout incorrecto. Necesaria id de usuario");
            resultado.msg = "Logout incorrecto";
            // aqui no puedo hacer el res.send({"msg": "Usuario incorrecto"}); porque se iria de la ejecución hay que ponerlo despues.
          }

      }

     res.send(resultado);

    }


module.exports.logoutV2 = logoutV2;
module.exports.loginV2 = loginV2;
