const io = require('../io');
const crypt = require ('../crypt');

const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucgt6ed/collections/";
const mLabAPIkey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";


//LOGIN V1 //ESCRIBO FICHERO
function loginV1(req, res) {
  console.log("POST /apitechu/v1/login");

  console.log("Body");
  console.log(req.body);
  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../users_pwds.json');
  var result = {};

  for (var user of users) {

    console.log("ID del usuario: " + user.id);
    console.log("email del usuario: " + user.email);
    console.log("pasword del usuario: " + user.pasword); //OJO el fichero tiene el texto "pasword" no "password"

    if (user.email == req.body.email && user.pasword == req.body.password ) {
      console.log("Email y Pass de usuario para LOGIN correcto " + user.email + " " + user.pasword);

      user.logged = true;

      io.writeUserDataToFile(users);

      console.log("Usuario persistido con logged con éxito en fichero");
      result.mensaje = "Login correcto";
      result.idUsuario = user.id;
      break;
    } else {
      result.mensaje = "Login incorrecto";
      }
  }
  res.send(result);
}

//LOGOUT V1 //ESCRIBO FICHERO
function logoutV1(req, res) {
  console.log("POST /apitechu/v1/logout");
  console.log("Body");
  console.log(req.body);
  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../users_pwds.json');
  var result = {};

  for (user of users) {
    console.log("us="+user.email);
    if (user.email == req.body.email && user.pasword == req.body.password && user.logged == true) {
      delete user.logged;
      result.mensaje = "Logout correcto";
      result.idUsuario = user.id;

      //user.logged=true;
      console.log("Logged Borrado en Logout");
      //console.log(users[user.id - 1].email);

      io.writeUserDataToFile(users);
      console.log("Logged Borrado en Logout PERSISTIDO");
      //res.send(user);
    break;
    } else {
      result.mensaje="Login incorrecto";
      }
  }
  res.send(result);
}

//LOGIN V2 //ESCRIBO BBDD
function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");
  var email = req.body.email;
  var password = req.body.password;

  var query = 'q={"email": "' + email + '"}';
  console.log("query es " + query);

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      var isPasswordcorrect =
        crypt.checkPassword(password, body[0].password);
      console.log("Password correct is " + isPasswordcorrect);

      if (body.length == 0 || !isPasswordcorrect) {
        var response = {
          "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
        }
        res.send(response);
      } else {
          console.log("Got a user with that email and password, logging in");
          query = 'q={"id" : ' + body[0].id +'}';
          console.log("Query for put is " + query);
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT done");
              var response = {
                "msg" : "Usuario logado con éxito",
                "idUsuario" : body[0].id
              }
              res.send(response);
            }
          )
        }
   }
  );
}


//LOGOUT V2 ESCRIBO BBDD
function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
module.exports.loginV2 = loginV2;
