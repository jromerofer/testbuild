const io = require('../io');

function loginV1(req, res) {
   console.log("POST /apitechu/v1/users/login");
   console.log("BODY pasado en llamada POSTMAN:");
   console.log(req.body);
   console.log(req.body.email);
   console.log(req.body.password);

   var users = require('../users_pwds.json');
   var resultado = {};

    for (var user of users) {

      console.log("ID del usuario: " + user.id);
      console.log("email del usuario: " + user.email);
      console.log("pasword del usuario: " + user.pasword); //OJO el fichero tiene el texto "pasword" no "password"
                                                           //Lo cambio aquí para no rehacer todo el ficherito...

      if (user.email == req.body.email && user.pasword == req.body.password) {

         console.log("Email y Pass de usuario para LOGIN correcto " + user.email + " " + user.pasword);

         user.logged = true;
         console.log("Logged de usuario a: " + user.logged );

         io.writeUserDataToFile(users);

         console.log("Usuario persistido con logged con éxito en fichero");

         resultado.msg = "Login correcto";
         resultado.idUsuario = user.id;

         // aqui no puedo hacer el res.send({"msg": "Login correcto"}); porque se iría de la ejecución, hay que ponerlo después.
         break;

      } else {
            console.log("No es posible validar el usuario");
            resultado.msg = "Login incorrecto";
      }

    }

    res.send(resultado);

  }


//LOGOUT
function logoutV1(req, res) {
    console.log("POST /apitechu/v1/users/logout");
    console.log("BODY pasado en llamada POSTMAN:");
    console.log(req.body);
    console.log(req.body.id);

    var users = require('../users_pwds.json');
    var resultado = {};

     for (var user of users) {

      console.log("ID del usuario: " + user.id);
      console.log("email del usuario: " + user.email);
      console.log("Pasword del usuario: " + user.pasword);

      // El ejecicio solo pide validar el "ID" pero creo que deberían validarse más parémetros... en el BODY, en este caso solo va el ID.
      // Dejo una posible valicación... OJO con la llave ;-) "{"

      //if (user.email == req.body.email && user.pasword == req.body.password && user.id == req.body.id) {
       if (user.id === req.body.id) {

          console.log("ID de usuario para LOGOUT correcto " + user.id);

          delete user.logged;
          console.log("Eliminado Logged de usuario ID: " + user.id);

          io.writeUserDataToFile(users);

          console.log("Usuario persistido sin logged con éxito en fichero");
          resultado.msg = "Logout correcto";
          resultado.idUsuario = user.id;

          break;

        } else {
            console.log("Logout incorrecto. Necesaria id de usuario");
            resultado.msg = "Logout incorrecto";
            // aqui no puedo hacer el res.send({"msg": "Usuario incorrecto"}); porque se iria de la ejecución hay que ponerlo despues.
          }

      }

     res.send(resultado);

    }


module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
